package com.evertsmits.parallel.computing.assignment4.common;

public interface Constants {

    String URL = "tcp://localhost:61616";
    String QUEUE_COMMON = "QUEUE.COMMON";
    String QUEUE_GENERATED = "QUEUE.GENERATED";
    String TOPIC_RANDOM = "TOPIC.RANDOM";
    String FOUND_PASSWORD = "FOUND.PASSWORD";
}
