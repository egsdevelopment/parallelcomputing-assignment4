package com.evertsmits.parallel.computing.assignment4;

import com.evertsmits.parallel.computing.assignment4.common.Constants;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;
import java.io.IOException;
import java.util.*;

public class Producer {
    private static Random random = new Random();
    private static javax.jms.MessageProducer producerCommon;
    private static javax.jms.MessageProducer producerRandom;
    private static javax.jms.MessageProducer producerGenerated;
    private static Session session;
    private static Connection connection;

    private static final ObjectMapper MAPPER = new ObjectMapper();


    public static void main(String[] args) {

        System.out.print("Enter the password to test: ");
        Scanner s = new Scanner(System.in);
        String password = s.nextLine();

        BruteForce b = new BruteForce(password);

        b.importMostCommon();

        final int NUMBER_OF_CONSUMERS = 10;
        int posPerChunkCommon = (int) Math.ceil(b.mostCommon.size() / NUMBER_OF_CONSUMERS);
        final long SEED = 8474839;
        random.setSeed(SEED);
        Collections.shuffle(b.mostCommon, random);

        // Generate the hash of the user input for testing

        try {
            // Create a ConnectionFactory
            ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(Constants.URL);

            // Create a Connection
            connection = connectionFactory.createConnection();
            connection.start();

            // Create a Session
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

            // Create the destination for common passwords (Topic or Queue)
            Destination destinationCommon = session.createQueue(Constants.QUEUE_COMMON);
            // Create the destination for Generated passwords
            Destination destinationGenerated = session.createQueue(Constants.QUEUE_GENERATED);
            // Create the destination for Random passwords
            Destination destinationRandom = session.createTopic(Constants.TOPIC_RANDOM);
            // Create destination for receiving answer from consumers
            Destination destinationDone = session.createQueue(Constants.FOUND_PASSWORD);

            MessageConsumer consumer = session.createConsumer(destinationDone);
            // Create a Producer for common passwords list from the Session to the Queue
            producerCommon = session.createProducer(destinationCommon);
            // Create a Producer for generated passwords list from the Session to the Queue
            producerGenerated = session.createProducer(destinationGenerated);
            // Create a Producer for random passwords from the Session to the Topic
            producerRandom = session.createProducer(destinationRandom);

            producerCommon.setDeliveryMode(DeliveryMode.PERSISTENT);
            producerGenerated.setDeliveryMode(DeliveryMode.PERSISTENT);
            producerRandom.setDeliveryMode(DeliveryMode.PERSISTENT);
            //Create a session text message

            //STRAT-1 ALWAYS CHECK COMMON PASSWORD LIST AND SEND QUEUE PACKETS FOR CONSUMERS TO CONSUME

            for (int i = 0; i < NUMBER_OF_CONSUMERS; i++) {
                List<String> check = b.mostCommon.subList(i * posPerChunkCommon, ((i * posPerChunkCommon) + posPerChunkCommon));
                SendObject so = new SendObject(check, b);
                TextMessage arrayToSend = createTextMessage(session, so);
                // Tell the producer to send the message
                producerCommon.send(arrayToSend);
            }

            if(password.length() <= 3){
                //STRAT-2
                b.generatePossibilities();

                int posPerChunk = (int) Math.ceil(b.possibilities.size() / NUMBER_OF_CONSUMERS);
                random.setSeed(SEED);
                Collections.shuffle(b.possibilities, random);

                for (int i = 0; i < NUMBER_OF_CONSUMERS; i++) {
                    List<String> check = b.possibilities.subList(i * posPerChunk, ((i * posPerChunk) + posPerChunk));
                    SendObject so = new SendObject(check, b);
                    TextMessage arrayToSend = createTextMessage(session, so);
                    producerGenerated.send(arrayToSend);
                }

            } else {
                //STRAT-3
                SendObject so = new SendObject(b);
                //Create a session text message
                TextMessage message = createTextMessage(session, so);
                // Tell the producer to send the message
                producerRandom.send(message);
            }
            Message message = consumer.receive();
            printDone(message);
            closeSession();
        }
        catch (JMSException  | IOException e) {
            throw new RuntimeException(e);
        }


    }

    private static TextMessage createTextMessage(Session session,SendObject parms) throws JMSException, JsonProcessingException {
        String jsonString = MAPPER.writeValueAsString(parms);
        return session.createTextMessage(jsonString);
    }

    private static void printDone(Message message) throws JMSException {
        TextMessage textMessage = (TextMessage) message;
        System.out.println(textMessage.getText());
    }

    private static void closeSession() throws JMSException {
        producerCommon.close();
        producerGenerated.close();
        producerRandom.close();
        session.close();
        connection.close();

    }


}
