package com.evertsmits.parallel.computing.assignment4;

import java.io.Serializable;

import java.util.List;

public class SendObject implements Serializable {
    private List<String> array;
    private BruteForce bruteForce;

    public SendObject(){}

    public SendObject(List<String> array, BruteForce bruteForce){
        this.array = array;
        this.bruteForce = bruteForce;
    }

    public SendObject(BruteForce bruteForce) {
        this.bruteForce = bruteForce;
    }

    public BruteForce getBruteForce() {
        return bruteForce;
    }

    public void setBruteForce(BruteForce bruteForce) {
        this.bruteForce = bruteForce;
    }

    public List<String> getArray() {
        return array;
    }

    public void setArray(List<String> array) {
        this.array = array;
    }
}
