package com.evertsmits.parallel.computing.assignment4;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BruteForce implements Serializable {
    public String passwordHash;
    private int passwordLength;
    private String password;
    private char[] alphabet = new char[]{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
    public List<String> possibilities;
    public List<String> mostCommon;

    public BruteForce(String password) {
        this.passwordHash = generateHash(password);
        this.passwordLength = password.length();
        this.possibilities = new ArrayList<>();
        this.mostCommon = new ArrayList<>();
        this.password = password;

    }

    public BruteForce(){}

    public String getPassword() {
        return this.password;
    }

    public void importMostCommon() {
        Scanner s = null;
        try {
            s = new Scanner(new File("passwords.txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        while (s.hasNext()) {
            mostCommon.add(s.next());
        }
        s.close();
    }

    public boolean testMostCommon(List<String> mostCommonPart) {
        boolean possibility = testPasswords(mostCommonPart);

        return possibility;
    }

    public boolean testRandom() {
        StringBuilder stringBuilder = new StringBuilder();
        boolean passwordFound = false;

        while (!passwordFound) {
            stringBuilder.setLength(0);
            for (int i = 0; i < getPasswordLength(); i++) {
                int random = new Random().nextInt(alphabet.length);

                stringBuilder.append(alphabet[random]);

            }
            System.out.println("CHECKING PASSWORD: " + stringBuilder.toString());
            passwordFound = testPassword(stringBuilder.toString());

        }

        return true;
    }

    public int getPasswordLength() {
        return passwordLength;
    }

    private boolean testPasswords(List<String> mostCommonPart) {
        for (String possibility : mostCommonPart) {
            if (testPassword(possibility)) {
                System.out.println("FOUND PASSWORD: " + possibility);
                return true;
            }
        }
        return false;
    }

    private boolean testPassword(String password) {
        String hash = generateHash(password);

        if (hash.equals(passwordHash)) {


            return true;
        }

        return false;
    }

    public String getHash() {
        return passwordHash;
    }

    public String[] getPossibilities() {
        return possibilities.toArray(new String[0]);
    }

    public static String generateHash(String passwordToHash) {
        String generatedPassword = null;

        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            byte[] bytes = md.digest(passwordToHash.getBytes(StandardCharsets.UTF_8));
            StringBuilder sb = new StringBuilder();

            for (byte aByte : bytes) {
                sb.append(Integer.toString((aByte & 0xff) + 0x100, 16).substring(1));
            }

            generatedPassword = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return generatedPassword;
    }

    public void generatePossibilities() {
        // Find all possible combinations of this alphabet in the string size of 3
        possibleStrings(passwordLength, alphabet, "");
    }

    private void possibleStrings(int maxLength, char[] alphabet, String curr) {
        // If the current string has reached it's maximum length
        if (curr.length() == maxLength) {
            possibilities.add(curr);

            // Else add each letter from the alphabet to new strings and process these new strings again
        } else {
            // Loop through each char in the alphabet
            for (char alphabetChar : alphabet) {
                String oldCurr = curr;
                curr += alphabetChar;
                possibleStrings(maxLength, alphabet, curr);
                curr = oldCurr;
            }
        }
    }
}
