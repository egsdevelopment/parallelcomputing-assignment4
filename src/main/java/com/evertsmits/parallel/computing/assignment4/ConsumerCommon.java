package com.evertsmits.parallel.computing.assignment4;

import com.evertsmits.parallel.computing.assignment4.common.Constants;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;
import java.io.IOException;

public class ConsumerCommon {
    public static boolean found = false;
    private static javax.jms.MessageConsumer consumer;
    private static Session session;
    private static Connection connection;
    private static javax.jms.MessageProducer producerDone;

    public static void main(String[] args) {
        try {
            long startTime = System.nanoTime();

            ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(Constants.URL);

            connection = connectionFactory.createConnection();
            connection.start();

            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

            Destination destination = session.createQueue(Constants.QUEUE_COMMON);

            // Create destination for receiving answer from consumers
            Destination destinationDone = session.createQueue(Constants.FOUND_PASSWORD);

            producerDone = session.createProducer(destinationDone);

            consumer = session.createConsumer(destination);

            ObjectMapper mapper = new ObjectMapper();

            Message message = consumer.receive();

            if (message instanceof TextMessage) {
                TextMessage textMessage = (TextMessage) message;
                String text = textMessage.getText();
                SendObject params = mapper.readValue(text, SendObject.class);

                System.out.println("going to try to find password: " + params.getBruteForce().getPassword() + " inside common passwords");

                found = params.getBruteForce().testMostCommon(params.getArray());
                if (found){
                    TextMessage done = session.createTextMessage("FOUND PASSWORD INSIDE ConsumerCommon");
                    // Tell the producer to send the message
                    producerDone.send(done);
                    outPutTime(startTime);
                    closeSession();
                } else {
                    System.out.println("COULD NOT FIND PASSWORD INSIDE COMMON PASSWORD LIST");
                    closeSession();
                }
            } else {
                System.out.println("Received message because type of input is wrong: " + message);
                closeSession();
            }
        } catch (JMSException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void outPutTime(long startTime) {
        // Output benchmark results
        long endTime = System.nanoTime();
        long duration = (endTime - startTime);
        System.out.println("Duration of the brute force: " + duration);
        System.exit(0);
    }

    public static void closeSession() throws JMSException {
        consumer.close();
        session.close();
        connection.close();
    }
}
